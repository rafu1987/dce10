# DCE-Extension for TYPO3

## What is DCE?

DCE is an extension for TYPO3 CMS, which creates easily and fast *dynamic content elements*.
Based on Extbase, Fluid and over 8 years of experience.


### Screenshots

![DCE General Configuration](Documentation/FirstSteps/Images/first-dce.png "DCE General Configuration")

![Inline Templating in DCE](Documentation/FirstSteps/Images/template-default.png "Inline Templating in DCE")


## Installation

You can install DCE in TYPO3 CMS using the [TER](https://extensions.typo3.org/extension/dce/) 
or use composer to fetch DCE from [packagist](https://packagist.org/packages/t3/dce):

```
composer req t3/dce:"^2.3"
```


## Documentation

The full documentation can be found here: https://docs.typo3.org/p/t3/dce/master/en-us/


## How to contribute?

Just fork this repository and create a pull request to the **master** branch.
Please also describe why you've submitted your patch. If you have any questions feel free to contact me.

In case you can't provide code but want to support DCE anyway, here is my [PayPal donation link](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=2DCCULSKFRZFU).

**Thanks to all contributors and sponsors!**
